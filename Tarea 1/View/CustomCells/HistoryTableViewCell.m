//
//  HistoryTableViewCell.m
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
