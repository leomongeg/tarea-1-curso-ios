//
//  AppDelegate.h
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 30/6/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;


@end

