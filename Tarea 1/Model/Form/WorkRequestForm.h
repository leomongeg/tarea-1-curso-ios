//
//  WorkRequestForm.h
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForms.h"

@interface WorkRequestForm : NSObject <FXForm>

@property (nonatomic, copy) NSString *brand;
@property (nonatomic, copy) NSString *clientName;
@property (nonatomic, strong) NSDate *model;
@property (nonatomic) float amount;
@property (nonatomic, copy) NSString *workDescription;

- (BOOL)isFormValid;
- (void)save;

@end
