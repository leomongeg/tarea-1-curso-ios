//
//  FilterOptions.h
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#ifndef Tarea_1_FilterOptions_h
#define Tarea_1_FilterOptions_h

typedef NS_ENUM(NSInteger, FilterActions) {
    FilterAll,
    FilterByDay,
    FilterByMonth
};

#endif
