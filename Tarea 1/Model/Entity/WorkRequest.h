#import "_WorkRequest.h"
#import "FilterOptions.h"

@interface WorkRequest : _WorkRequest {}

+ (NSPredicate *)historyPredicateWithDate:(NSDate *)date andFilter:(FilterActions)filter;
+ (double)totalAmountForDate:(NSDate *)date andFilter:(FilterActions)filter;

@end
