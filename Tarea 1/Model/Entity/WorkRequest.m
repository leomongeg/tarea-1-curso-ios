#import "WorkRequest.h"

@interface WorkRequest ()

@end

@implementation WorkRequest

#pragma mark - Predicates for history table view

+ (NSPredicate *)historyPredicateWithDate:(NSDate *)date andFilter:(FilterActions)filter
{
    
    NSDate *startDate      = [WorkRequest startDate:date andFilter:filter];
    NSDate *endDate        = [WorkRequest endDate:date andFilter:filter];    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"createdAt >= %@ AND createdAt < %@", startDate, endDate];
    
    return predicate;
}

#pragma mark - Datetime functions for predicates

+ (NSDate *)startDate:(NSDate *)date andFilter:(FilterActions)filter
{
    NSCalendar *cal               = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    NSDateComponents * components = [cal components:( NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    
    [components setHour:00];
    [components setMinute:00];
    
    if(filter == FilterByMonth)
    {
        [components setDay:01];
    }
    
    return [cal dateFromComponents:components];
}

+ (NSDate *)endDate:(NSDate *)date andFilter:(FilterActions)filter
{
    NSCalendar *cal              = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    NSUInteger componentFlags    = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *components = [cal components:componentFlags fromDate:date];
    NSRange rng                  = [cal rangeOfUnit:NSDayCalendarUnit
                                             inUnit:NSMonthCalendarUnit forDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    
    if(filter == FilterByMonth)
    {
        [components setDay:rng.length];
    }
    
    return [cal dateFromComponents:components];
}

#pragma mark - Predicates for total amount calculation

+ (NSPredicate *)totalAmountPredicateForDate:(NSDate *)date andFilter:(FilterActions)filter
{
    NSDate *startDate               = [WorkRequest startDate:date andFilter:filter];
    NSDate *endDate                 = [WorkRequest endDate:date andFilter:filter];
    
    NSPredicate *predicate          = [NSPredicate predicateWithFormat:@"createdAt >= %@ AND createdAt < %@", startDate, endDate];
    
    return predicate;
}

+ (double)totalAmountForDate:(NSDate *)date andFilter:(FilterActions)filter
{
    
    NSFetchRequest *request         = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity     = [NSEntityDescription entityForName:@"WorkRequest"
                                                  inManagedObjectContext:[NSManagedObjectContext MR_defaultContext]];
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath:@"amount"];
    NSExpression *sumExpression     = [NSExpression expressionForFunction:@"sum:"
                                                                arguments:[NSArray arrayWithObject:keyPathExpression]];
    if(filter != FilterAll)
    {
        [request setPredicate:[self totalAmountPredicateForDate:date andFilter:filter]];
    }
    
    NSExpressionDescription *expTotal = [[NSExpressionDescription alloc] init];
    [request setEntity:entity];
    [request setResultType:NSDictionaryResultType];
    
    [expTotal setName:@"total"];
    [expTotal setExpression:sumExpression];
    [expTotal setExpressionResultType:NSDoubleAttributeType];
    [request setPropertiesToFetch:@[expTotal]];
    
    NSError *error;
    NSArray *objects = [[NSManagedObjectContext MR_defaultContext] executeFetchRequest:request error:&error];
    
    if(error)
    {
        return 0;
    }
    
    NSDictionary *data = [objects objectAtIndex:0];
    
    return [[data objectForKey:@"total"] doubleValue];
}


@end
