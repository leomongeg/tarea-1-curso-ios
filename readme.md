Tarea Nº 1
===================

**Jorge Leonardo Monge García**

**Curso iOS Avanzado CENFOTEC**

Instrucciones
----------------
El proyecto utiliza cocoapods, para integrar algunas librerías de terceros. Los pods se encuentran ignorados del repositorio de versionamiento, por lo que, para que el proyecto compile y funcione correctamente, hay que ejecutar los siguientes pasos:

> **Pasos:**

> *  Abrir un terminal y dirigirse al directorio donde se clonó el repositorio.

> * Ejecutar el comando pod install

> * Ejecutar el proyecto