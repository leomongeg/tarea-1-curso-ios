//
//  WorkRequestFormViewController.m
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "WorkRequestFormViewController.h"
#import "WorkRequestForm.h"
#import "WorkRequest.h"

@interface WorkRequestFormViewController ()

@end

@implementation WorkRequestFormViewController

- (void)awakeFromNib
{
    self.formController.form = [WorkRequestForm new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)submitRegistrationForm:(UITableViewCell<FXFormFieldCell> *)cell
{
    WorkRequestForm *form = (WorkRequestForm *) cell.field.form;
    NSLog(@"%@", form.brand);
    
    if(![form isFormValid])
    {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"Todos los datos son requeridos"
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Aceptar", nil] show];
        return;
    }
    
    [form save];
    
    [self cancelRegistrationForm:nil];
}

- (void)cancelRegistrationForm:(UITableViewCell<FXFormFieldCell> *)cell
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
