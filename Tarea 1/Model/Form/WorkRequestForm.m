//
//  WorkRequestForm.m
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "WorkRequestForm.h"
#import "WorkRequest.h"

@implementation WorkRequestForm

#pragma mark - Form config

- (NSArray *)fields
{
    return @[
             @{FXFormFieldKey: @"brand", FXFormFieldHeader: @"Nuevo trabajo", FXFormFieldTitle: @"Marca"},
             @{FXFormFieldKey: @"model", FXFormFieldTitle: @"Modelo", FXFormFieldType: FXFormFieldTypeDate},
             @{FXFormFieldKey: @"clientName", FXFormFieldTitle: @"Cliente"},
             @{FXFormFieldKey: @"amount", FXFormFieldTitle: @"Monto", FXFormFieldType: FXFormFieldTypeFloat},
             @{FXFormFieldKey: @"workDescription", FXFormFieldTitle: @"Descripción", FXFormFieldType: FXFormFieldTypeLongText},
             @{FXFormFieldTitle: @"Guardar", FXFormFieldHeader: @"", FXFormFieldAction: @"submitRegistrationForm:"},
             @{FXFormFieldTitle: @"Cancelar", FXFormFieldHeader: @"", FXFormFieldAction: @"cancelRegistrationForm:"}             
             ];
}

#pragma mark - Validation methods

- (BOOL)isFormValid
{
    if (!self.brand      || !self.model  ||
        !self.clientName || !self.amount ||
        !self.workDescription || self.amount <= 0)
    {
        return NO;
    }
    
    return YES;
}

#pragma mark - Persistens methods

- (void)save
{
    WorkRequest *workRequest = [WorkRequest MR_createEntity];
    
    [workRequest setBrand:self.brand];
    [workRequest setModelValue:[self getModelYear]];
    [workRequest setAmountValue:self.amount];
    [workRequest setClientName:self.clientName];
    [workRequest setWorkDescription:self.workDescription];
    [workRequest setCreatedAt:[NSDate date]];
    
    [workRequest.managedObjectContext MR_saveToPersistentStoreAndWait];
}

- (int)getModelYear
{
    NSCalendar* calendar         = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.model];
    
    return [components year];
}

@end
