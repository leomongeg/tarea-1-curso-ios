//
//  WorkHistoryViewController.m
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "WorkHistoryViewController.h"
#import "HistoryTableViewCell.h"
#import "WorkRequest.h"
#import "ActionSheetDatePicker.h"

#define CELL_IDENTIFIER @"WorkRequest"

@interface WorkHistoryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic) FilterActions currentFilterAction;
@property (nonatomic) NSDate *currentFilterDate;
@property (weak, nonatomic) IBOutlet UIButton *btnShowAll;
@property (weak, nonatomic) IBOutlet UIButton *btnByDay;
@property (weak, nonatomic) IBOutlet UIButton *btnByMonth;
@property (nonatomic) double totalAmount;

@end

@implementation WorkHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tblWorkHystory setDelegate:self];
    [self.tblWorkHystory setDataSource:self];
    self.totalAmount = [WorkRequest totalAmountForDate:self.currentFilterDate andFilter:self.currentFilterAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self reloadTableData];
    [self setFilterSeleted:self.btnShowAll];
}

#pragma mark - Call to action

- (IBAction)doFilterAll:(id)sender
{
    self.currentFilterAction = FilterAll;
    [self reloadTableData];
    [self setFilterSeleted:sender];
}

- (IBAction)doFilterByDay:(id)sender
{
    self.currentFilterAction = FilterByDay;
    [self showFilterByDate];
    [self setFilterSeleted:sender];
}

- (IBAction)doFilterByMonth:(id)sender
{
    self.currentFilterAction = FilterByMonth;
    [self showFilterByDate];
    [self setFilterSeleted:sender];
}

- (void)setFilterSeleted:(UIButton *)seleted
{
    [_btnShowAll setBackgroundColor:[UIColor colorWithHexString:@"7986CB"]];
    [_btnByMonth setBackgroundColor:[UIColor colorWithHexString:@"7986CB"]];
    [_btnByDay   setBackgroundColor:[UIColor colorWithHexString:@"7986CB"]];
    
    [seleted setBackgroundColor:[UIColor colorWithHexString:@"3F51B5"]];
}

- (IBAction)goBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [NSString stringWithFormat:@"Monto total: %.02f", self.totalAmount];
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell *cell = (HistoryTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    WorkRequest *work = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [cell.lblName setText:work.clientName];
    [cell.lblBrand setText:work.brand];
    [cell.lblModel setText:[work.model stringValue]];
    [cell.lblYear setText:[NSString stringWithFormat:@"Monto: %f", [work.amount floatValue]]];
    
    
    return cell;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [WorkRequest MR_requestAllSortedBy:@"createdAt" ascending:NO];
    
    if(self.currentFilterAction != FilterAll)
    {
        [fetchRequest setPredicate:[WorkRequest historyPredicateWithDate:self.currentFilterDate andFilter:self.currentFilterAction]];
    }
    
    [fetchRequest setFetchLimit:100];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                  managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                                                                    sectionNameKeyPath:nil
                                                                                                             cacheName:@"TBLWorkReqCache"];
    _fetchedResultsController = theFetchedResultsController;
    
    return _fetchedResultsController;
}

-(void)reloadTableData
{
    [NSFetchedResultsController deleteCacheWithName:@"TBLWorkReqCache"];
    _fetchedResultsController = nil;
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    self.totalAmount = [WorkRequest totalAmountForDate:self.currentFilterDate andFilter:self.currentFilterAction];
    [self.tblWorkHystory reloadData];
}

- (void)showFilterByDate
{
    @weakify(self);
    [ActionSheetDatePicker showPickerWithTitle:@"Fecha"
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:self.currentFilterDate ? self.currentFilterDate : [NSDate date]
                                   minimumDate:nil
                                   maximumDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin){
                                         
                                         @strongify(self);
                                         self.currentFilterDate = selectedDate;
                                         
                                         [self reloadTableData];
                                         
                                     }
                                   cancelBlock:^(ActionSheetDatePicker *picker){}
                                        origin:self.view];
}

@end
