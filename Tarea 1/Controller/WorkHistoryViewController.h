//
//  WorkHistoryViewController.h
//  Tarea 1
//
//  Created by Jorge Leonardo Monge García on 5/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkHistoryViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblWorkHystory;

@end
