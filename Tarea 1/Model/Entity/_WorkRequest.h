// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WorkRequest.h instead.

@import CoreData;

extern const struct WorkRequestAttributes {
	__unsafe_unretained NSString *amount;
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *clientName;
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *model;
	__unsafe_unretained NSString *workDescription;
} WorkRequestAttributes;

@interface WorkRequestID : NSManagedObjectID {}
@end

@interface _WorkRequest : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) WorkRequestID* objectID;

@property (nonatomic, strong) NSNumber* amount;

@property (atomic) double amountValue;
- (double)amountValue;
- (void)setAmountValue:(double)value_;

//- (BOOL)validateAmount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* brand;

//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* clientName;

//- (BOOL)validateClientName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* createdAt;

//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* model;

@property (atomic) int32_t modelValue;
- (int32_t)modelValue;
- (void)setModelValue:(int32_t)value_;

//- (BOOL)validateModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* workDescription;

//- (BOOL)validateWorkDescription:(id*)value_ error:(NSError**)error_;

@end

@interface _WorkRequest (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAmount;
- (void)setPrimitiveAmount:(NSNumber*)value;

- (double)primitiveAmountValue;
- (void)setPrimitiveAmountValue:(double)value_;

- (NSString*)primitiveBrand;
- (void)setPrimitiveBrand:(NSString*)value;

- (NSString*)primitiveClientName;
- (void)setPrimitiveClientName:(NSString*)value;

- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;

- (NSNumber*)primitiveModel;
- (void)setPrimitiveModel:(NSNumber*)value;

- (int32_t)primitiveModelValue;
- (void)setPrimitiveModelValue:(int32_t)value_;

- (NSString*)primitiveWorkDescription;
- (void)setPrimitiveWorkDescription:(NSString*)value;

@end
