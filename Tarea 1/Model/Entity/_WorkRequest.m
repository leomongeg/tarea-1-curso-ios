// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WorkRequest.m instead.

#import "_WorkRequest.h"

const struct WorkRequestAttributes WorkRequestAttributes = {
	.amount = @"amount",
	.brand = @"brand",
	.clientName = @"clientName",
	.createdAt = @"createdAt",
	.model = @"model",
	.workDescription = @"workDescription",
};

@implementation WorkRequestID
@end

@implementation _WorkRequest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"WorkRequest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"WorkRequest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"WorkRequest" inManagedObjectContext:moc_];
}

- (WorkRequestID*)objectID {
	return (WorkRequestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"amountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"amount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"modelValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"model"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic amount;

- (double)amountValue {
	NSNumber *result = [self amount];
	return [result doubleValue];
}

- (void)setAmountValue:(double)value_ {
	[self setAmount:@(value_)];
}

- (double)primitiveAmountValue {
	NSNumber *result = [self primitiveAmount];
	return [result doubleValue];
}

- (void)setPrimitiveAmountValue:(double)value_ {
	[self setPrimitiveAmount:@(value_)];
}

@dynamic brand;

@dynamic clientName;

@dynamic createdAt;

@dynamic model;

- (int32_t)modelValue {
	NSNumber *result = [self model];
	return [result intValue];
}

- (void)setModelValue:(int32_t)value_ {
	[self setModel:@(value_)];
}

- (int32_t)primitiveModelValue {
	NSNumber *result = [self primitiveModel];
	return [result intValue];
}

- (void)setPrimitiveModelValue:(int32_t)value_ {
	[self setPrimitiveModel:@(value_)];
}

@dynamic workDescription;

@end

